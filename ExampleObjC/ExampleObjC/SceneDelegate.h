//
//  SceneDelegate.h
//  ExampleObjC
//
//  Created by George on 12.04.2021.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

