//
//  LoginViewController.m
//  ExampleObjC
//
//  Created by George on 12.04.2021.
//

#import "LoginViewController.h"
#import "StreamController.h"

@interface LoginViewController () <UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate> {
    NSString *streamLink;
    NSString *accessToken;
}
@property (weak, nonatomic) IBOutlet UITextField *groupIDTextField;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // MARK: Add your own method to get access token
    streamLink = @"https://demo-app.sceenic.co/football.m3u8";
    accessToken = @"";
    NSArray *textFields = @[
                            self.groupIDTextField,
    ];
    for (UITextField *field in textFields) {
        field.layer.borderColor = [[UIColor systemBlueColor] CGColor];
        field.layer.borderWidth = 0.5;
        field.layer.cornerRadius = 5.0;
        field.delegate = self;
    }
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    [tapGesture addTarget:self action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
}

- (void)hideKeyboard {
    [self.view endEditing:YES];
}

- (IBAction)loginAction:(id)sender {
    if (self.groupIDTextField.text.length == 0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Mandatory fields" message:@"Fields can't be empty" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    UIStoryboard* story = [self storyboard];
    StreamController* vc = [story instantiateViewControllerWithIdentifier:@"StreamController"];
    vc.groupId = self.groupIDTextField.text;
    vc.streamURL = streamLink;
    vc.token = accessToken;
    [self.navigationController pushViewController:vc animated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return  YES;
}

- (NSInteger)numberOfComponentsInPickerView:(nonnull UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(nonnull UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return 2;
}

@end
