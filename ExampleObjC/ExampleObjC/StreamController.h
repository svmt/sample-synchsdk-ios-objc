//
//  StreamController.h
//  ExampleObjC
//
//  Created by George on 12.04.2021.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface StreamController : UIViewController
@property (nonatomic, strong) NSString* roomName;
@property (nonatomic, strong) NSString* groupId;
@property (nonatomic, strong) NSString* token;
@property (nonatomic, strong) NSString* streamURL;

@end

NS_ASSUME_NONNULL_END
