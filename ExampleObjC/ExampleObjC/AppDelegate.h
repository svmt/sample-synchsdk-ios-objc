//
//  AppDelegate.h
//  ExampleObjC
//
//  Created by George on 12.04.2021.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

