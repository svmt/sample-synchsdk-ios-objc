//
//  StreamController.m
//  ExampleObjC
//
//  Created by George on 12.04.2021.
//

#import "StreamController.h"
@import SynchSDKAdapter;
@import AVKit;
#import <CallKit/CallKit.h>

@interface StreamController () <SynchListenerAdapter, CXCallObserverDelegate>
@property (weak, nonatomic) IBOutlet UIView *playerView;
@property (strong, nonatomic) AVPlayerLayer* playerLayer;
@property (nonatomic, strong) AVPlayer* player;
@property (nonatomic, strong) SynchSDKAdapter* synch;
@property (nonatomic, strong) NSMutableArray* clientList;
@property (weak, nonatomic) IBOutlet UILabel *accuracyLabel;
@property (weak, nonatomic) IBOutlet UILabel *deltaLabel;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIButton *pauseButton;
@property (weak, nonatomic) IBOutlet UIButton *syncOnButton;
@property (weak, nonatomic) IBOutlet UILabel *rateLabel;
@property (weak, nonatomic) IBOutlet UIButton *syncOffButton;
@property (weak, nonatomic) IBOutlet UILabel *clientsLabel;
@end

@implementation StreamController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self startPlayer];
    NSError *error = nil;
    self.synch = [[SynchSDKAdapter alloc] initWithAccessToken:self.token username:self.groupId error:&error];
    self.clientList = [[NSMutableArray alloc] init];
    [self.synch attachListener: self];
    [self.syncOffButton setHidden:YES];
    [self.pauseButton setEnabled:NO];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController.navigationBar setHidden:YES];
}

- (void)setupObservers {
    NSNotificationCenter* defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self selector:@selector(appMovedToBackground:) name: UIApplicationDidEnterBackgroundNotification object:nil];
    [defaultCenter addObserver:self selector:@selector(appDidBecomeActive:) name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        CGRect viewFrame = self.playerView.frame;
        viewFrame.size = size;
        [self.playerView setFrame:viewFrame];
        CGRect layerFrame = self.playerLayer.frame;
        layerFrame.size = size;
        [self.playerLayer setFrame:layerFrame];
    } completion:nil];
}

- (IBAction)goBackTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) startPlayer {
    NSURL* url = [NSURL URLWithString: self.streamURL];
    AVAsset* asset = [AVAsset assetWithURL: url];
    AVPlayerItem* item = [AVPlayerItem playerItemWithAsset:asset];
    item.audioTimePitchAlgorithm = AVAudioTimePitchAlgorithmVarispeed;
    self.player = [AVPlayer playerWithPlayerItem:item];
    self.playerLayer = [AVPlayerLayer playerLayerWithPlayer:_player];
    self.playerLayer.frame = self.playerView.bounds;
    [self.playerLayer setVideoGravity:AVLayerVideoGravityResizeAspect];
    [self.playerView.layer addSublayer:self.playerLayer];
}

- (IBAction)startSynch:(UIButton*)sender {
    [self.synch startSynchronize];
    [sender setHidden:YES];
    [self.syncOffButton setHidden:NO];
}

- (IBAction)stopSynch:(UIButton*)sender {
    [self.synch stopSynchronize];
    [sender setHidden:YES];
    [self.syncOnButton setHidden:NO];
}

- (IBAction)playerSeekBackward:(id)sender {
    CMTime time =  [_player currentItem].currentTime;
    double sec = CMTimeGetSeconds(time) - 15;
    [self.player seekToTime: CMTimeMake(sec, 1)];

    [self.synch playerSeekWithPosition: (NSInteger)sec * 1000];
}
- (IBAction)playerSeekForward:(id)sender {
    CMTime time =  [self.player currentItem].currentTime;
    double sec = CMTimeGetSeconds(time) + 15;
    [self.player seekToTime: CMTimeMake(sec, 1)];

    [self.synch playerSeekWithPosition: (NSInteger)sec * 1000];
}

- (IBAction)playerPause:(id)sender {
    [self.player pause];
    [self.synch playerPause];
    [sender setEnabled:NO];
    [self.playButton setEnabled:YES];
}

- (IBAction)playerPlay:(id)sender {
    [self.player play];
    [self.synch playerPlay];
    [sender setEnabled:NO];
    [self.pauseButton setEnabled:YES];
}

- (void)onClientListWithClientList:(NSArray<ClientAdapter *> * _Nonnull)clientList {
    NSLog(@"%@", clientList);
    self.clientList = [NSMutableArray arrayWithArray:clientList];
    NSString *clientsStr = @"";
    int i = 0;
    for (ClientAdapter *client in self.clientList) {
        clientsStr = [NSString stringWithFormat:@"%@%@%@", clientsStr, [client name], self.clientList.count > (i + 1) ? @"\n" : @"" ];
        i++;
    }
    [self.clientsLabel setText:clientsStr];
}

- (float)onGetPlaybackRate {
    return self.player.rate;
}

- (NSInteger)onGetPlayerPosition {
    NSDate* date = [self.player.currentItem currentDate];
    if (!(date == NULL || date == 0)) {
        double timeInterval = [date timeIntervalSince1970]*1000;
        return (NSInteger)timeInterval;
    } else {
        CMTime cmtime = [self.player.currentItem currentTime];
        NSInteger seconds = CMTimeGetSeconds(cmtime);
        return (seconds * 1000);
    }
}

- (void)onPlaybackFromPositionWithPosition:(NSInteger)position participantId:(NSString*) participantId {
    NSLog(@"onPlaybackFromPositionWithPosition participantId --> %@", participantId);
    NSString * name = [self getClientNameWith: participantId];
    NSLog(@"Name ==> %@", name);

    double seconds = (double)position/1000;
    CMTime time = CMTimeMake(seconds, 1);
    [self.player seekToTime:time];
}

- (void)onSetPlaybackRateWithRate:(float)rate {
    if ([self.player rate] != 0) {
        [self.player setRate:rate];
    }
}

- (void)onSyncInfoWithAccuracy:(float)accuracy delta:(NSInteger)delta {
    NSLog(@"%f %ld", accuracy, (long)delta);
    [self.accuracyLabel setText:[NSString stringWithFormat:@"%f", accuracy]];
    [self.deltaLabel setText:[NSString stringWithFormat:@"%ld", (long)delta]];
    [self.rateLabel setText:[NSString stringWithFormat:@"%f", _player.rate]];
}

- (void)onPauseWithParticipantId:(NSString *)participantId {
    NSLog(@"onPauseWithParticipantId participantId --> %@", participantId);
    NSString * name = [self getClientNameWith: participantId];
    NSLog(@"Name ==> %@", name);
    [self.pauseButton setEnabled:NO];
    [self.playButton setEnabled:YES];
    [self.player pause];
}

- (void)onResumePlayWithParticipantId:(NSString *)participantId {
    NSLog(@"onResumePlayWithParticipantId participantId --> %@", participantId);
    NSString * name = [self getClientNameWith: participantId];
    NSLog(@"Name ==> %@", name);
    [self.pauseButton setEnabled:YES];
    [self.playButton setEnabled:NO];
    [self.player play];
}

- (NSString *)getClientNameWith:(NSString *)participantId {
    //MARK: if sender == @"" it's server initiated event
    for (int i = 0; i < [self.clientList count]; i++) {
        ClientAdapter *client = [self.clientList objectAtIndex: i];
        if ([participantId isEqualToString:client.id]) {
            return client.name;
        }
    }
    return @"server";
}

// MARK: Sync on/off after background or after call
- (void)callObserver:(CXCallObserver *)callObserver callChanged:(CXCall *)call {
    if (call.hasEnded) {
        [self callEnded];
    } else {
        [self callStarted];
    }
}

- (void)callEnded {
    [self reconnectFlow];
}

- (void)callStarted {
    [self disconnectFlow];
}
- (void)appMovedToBackground:(NSNotification *) notification {
    [self disconnectFlow];
}

- (void)appDidBecomeActive:(NSNotification *) notification {
    [self reconnectFlow];
}

- (void)reconnectFlow {
    [self.synch startSynchronize];
}

- (void)disconnectFlow {
    [self.synch stopSynchronize];
}

@end
